
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = require('express')();
const server = require('http').createServer(app);
const config = require('./config');
////////////////////////////////Websockets//////////////////////////
require('./controllers/record-controller.js').sockets(server);
////////////////////////////////////////////////////////////////////
mongoose.Promise = global.Promise;
mongoose.connect(config.BASE_URL);
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit:'100mb', type:'application/json'}));

app.use(bodyParser.urlencoded({ extended:true, limit: '100mb'}));
app.use(cookieParser());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', false);
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Authorization, Cache-Control, Content-Type');
    if ('OPTIONS' == req.method) return res.status(200).send();
    next();
});
//require("./config/routes")(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handle
app.use(function(err, req, res, next) {

    console.log(err);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.send(err);
});

server.listen(config.PORT,function () {
    console.log("----server started---- on port=>",config.PORT);
});
