const db = require('../models');
const path = require('path')
const fs = require('fs')
const uuidv4 = require('uuid/v4');
var _ = require('underscore')
var connections = {};
//data-structure to list the connections 
module.exports.sockets = function(server) {
    let io = require('socket.io')(server);
    io.on('connection', function(socket) {
        //  console.log("user-agent: " + JSON.stringify(socket.request.headers));
        console.log("================socket client connected============================")
        socket.on('start-session', function(data) {
            console.log("============start-session event================")
            console.log(data)
            var session_id = uuidv4();
            //generating the sessions_id and then binding that socket to that sessions 
            socket.room = session_id;
            socket.join(socket.room, function(res) {
                console.log("joined successfully ")
                socket.site_id = data.site_id;
                socket.connection_type = "web-user"; //browser users


                if (connections[data.site_id]) {
                    if (connections[data.site_id]["siteId"]) {
                        if (connections[data.site_id]["siteUrl"]) {
                            if (connections[data.site_id]["sessions"]) {
                                if (connections[data.site_id]["sessions"][session_id]) {
                                    connections[data.site_id]["sessions"][session_id]["browser"] = "chrome/mozilla"
                                    connections[data.site_id]["sessions"][session_id]["os"] = "linux/windows"
                                    connections[data.site_id]["sessions"][session_id]["fullSnapShot"] = [] //fullsnapshots events with key and value
                                    connections[data.site_id]["sessions"][session_id]["streamShots"] = [] //fullsnapshot only to render the initial DOM at streaming
                                } else {
                                    connections[data.site_id]["sessions"][session_id] = {};
                                    connections[data.site_id]["sessions"][session_id]["sessionId"] = session_id
                                    connections[data.site_id]["sessions"][session_id]["browser"] = "chrome/mozilla"
                                    connections[data.site_id]["sessions"][session_id]["os"] = "linux/windows"
                                    connections[data.site_id]["sessions"][session_id]["fullSnapShot"] = [] //fullsnapshots events with key and value
                                    connections[data.site_id]["sessions"][session_id]["streamShots"] = [] //fullsnapshot only to render the initial DOM at streaming
                                }
                            } else {
                                connections[data.site_id]["sessions"] = {};
                                connections[data.site_id]["sessions"][session_id] = {};
                                connections[data.site_id]["sessions"][session_id]["sessionId"] = session_id
                                connections[data.site_id]["sessions"][session_id]["browser"] = "chrome/mozilla"
                                connections[data.site_id]["sessions"][session_id]["os"] = "linux/windows"
                                connections[data.site_id]["sessions"][session_id]["fullSnapShot"] = [] //fullsnapshots events with key and value
                                connections[data.site_id]["sessions"][session_id]["streamShots"] = [] //fullsnapshot only to render the initial DOM at streaming
                            }
                        } else {
                            connections[data.site_id]["siteUrl"] = data.siteUrl;
                            connections[data.site_id]["sessions"] = {};
                            connections[data.site_id]["sessions"][session_id] = {};
                            connections[data.site_id]["sessions"][session_id]["sessionId"] = session_id
                            connections[data.site_id]["sessions"][session_id]["browser"] = "chrome/mozilla"
                            connections[data.site_id]["sessions"][session_id]["os"] = "linux/windows"
                            connections[data.site_id]["sessions"][session_id]["fullSnapShot"] = [] //fullsnapshots events with key and value
                            connections[data.site_id]["sessions"][session_id]["streamShots"] = [] //fullsnapshot only to render the initial DOM at streaming
                        }
                    } else {
                        connections[data.site_id]["siteId"] = data.site_id;
                        connections[data.site_id]["siteUrl"] = data.siteUrl;
                        connections[data.site_id]["sessions"] = {};
                        connections[data.site_id]["sessions"][session_id] = {};
                        connections[data.site_id]["sessions"][session_id]["sessionId"] = session_id
                        connections[data.site_id]["sessions"][session_id]["browser"] = "chrome/mozilla"
                        connections[data.site_id]["sessions"][session_id]["os"] = "linux/windows"
                        connections[data.site_id]["sessions"][session_id]["fullSnapShot"] = [] //fullsnapshots events with key and value
                        connections[data.site_id]["sessions"][session_id]["streamShots"] = [] //fullsnapshot only to render the initial DOM at streaming
                    }
                } else {
                    connections[data.site_id] = {}
                    connections[data.site_id]["siteId"] = data.site_id;
                    connections[data.site_id]["siteUrl"] = data.siteUrl;
                    connections[data.site_id]["sessions"] = {};
                    connections[data.site_id]["sessions"][session_id] = {};
                    connections[data.site_id]["sessions"][session_id]["sessionId"] = session_id
                    connections[data.site_id]["sessions"][session_id]["browser"] = "chrome/mozilla"
                    connections[data.site_id]["sessions"][session_id]["os"] = "linux/windows"
                    connections[data.site_id]["sessions"][session_id]["fullSnapShot"] = [] //fullsnapshots events with key and value
                    connections[data.site_id]["sessions"][session_id]["streamShots"] = [] //fullsnapshot only to render the initial DOM at streaming
                }
            });
            //this will be a single entry once after that the data will be pushed in arrays 
        });
        socket.on('session-record', function(data) {
            console.log("======================start-record event==========" + socket.room)

            var obj = JSON.parse(data.event)
            var tempObj = {}
            tempObj[obj.timestamp] = obj

            if (obj.type == 2 || obj.type == 4) //full snapshot events
            {
                connections[data.site_id]["sessions"][socket.room]["streamShots"].push(obj) //added to test the streaming functionality
                tempObj["incrementalSnapshot"] = []
                connections[data.site_id]["sessions"][socket.room]["fullSnapShot"].push(tempObj)
                tempArray = connections[data.site_id]["sessions"][socket.room]["fullSnapShot"]

            } else {
                tempArray = connections[data.site_id]["sessions"][socket.room]["fullSnapShot"]
                tempArray[tempArray.length - 1]["incrementalSnapshot"].push(tempObj)
            }
            io.to(socket.room).emit('live-streamed-session', obj);
            //  emit the events to the room contineously for live stream purpose 

        });
        //event to provide the list of admins live-streams
        socket.on('get-streamlist', function(data) {
            //here the customer will recive live data. 
            var live_streamings = []
            var site_id = "uniqueTest";
            //  var site_id = data.site_id

            if (connections[site_id]) {
                var temp = connections[site_id].sessions
                for (var sessions in temp) {

                    var stream_list = {
                        "sessionId": temp[sessions].sessionId,
                        "browser": temp[sessions].browser,
                        "os": temp[sessions].os
                    }
                    live_streamings.push(stream_list)
                }
                socket.emit('view-streamlist', live_streamings);
            } else {
                console.log("=============no stremaings found=================")
                socket.emit('view-streamlist', []);
            }


        });
        //  dasbord-admin joins the live streaming to watch it 
        socket.on('view-live-session', function(data) {
            console.log(data.site_id)
            socket.join(data.sessionId, function(res) {
                if (connections[data.site_id]["sessions"][data.sessionId]["streamShots"]) {
                    console.log(JSON.stringify(connections))
                    console.log(data.sessionId)
                    var stream_initialiser = connections[data.site_id]["sessions"][data.sessionId]["streamShots"]
                    console.log(stream_initialiser.length)
                    console.log(data.site_id + "  dashbord admin joined successfully ")
                    socket.emit("initiate-stream", stream_initialiser)
                }

            });
        });

        socket.on('disconnect', function() {
            console.log("==================client disconnected==================")
                //the sessions will be saved after the connection closed 
            if (socket.connection_type == "web-user") {
                console.log("room    " + socket.room)
                console.log("site id   " + socket.site_id)
                if (connections[socket.site_id]["sessions"][socket.room]) {
                    let tempObj = {}
                    tempObj.site_id = connections[socket.site_id].siteId;
                    tempObj.site_url = connections[socket.site_id].siteUrl;
                    tempObj.session_id = connections[socket.site_id]["sessions"][socket.room].sessionId;
                    tempObj.browser = connections[socket.site_id]["sessions"][socket.room].browser;
                    tempObj.os = connections[socket.site_id]["sessions"][socket.room].os;
                    tempObj.events_snapshots = connections[socket.site_id]["sessions"][socket.room].fullSnapShot;
                    console.log("going to save the data")
                    db['recordings'].create(tempObj).then(res => {
                        console.log(res)
                        console.log("recording saved successfully")

                        delete connections[socket.site_id]["sessions"][socket.room]; //delete the temporary storage of sesssions events 
                        console.log(connections)
                    }).catch(err => {
                        console.log(err)
                    })
                } else {
                    console.log("data not found")
                }
            } else {
                console.log("not a web user client")
            }
        })
    })
};