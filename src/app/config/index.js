'use strict';

module.exports = (function (env) {
    let config = {};
    switch (env) {
        case 'production':
            config = require('../env/production');
            break;
        case 'development':
            config = require('../env/development');
            break;
        default:
            console.error('NODE_ENV environment variable not set');
            process.exit(1);
    }
    return config;
})(process.env.NODE_ENV);


