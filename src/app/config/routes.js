let express = require("express");

module.exports = function(app) {

    const router = express.Router();
    app.use('/public', express.static('public'));
    app.use(router);
};