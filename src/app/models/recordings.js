var mongoose = require('mongoose');
var paginate = require('mongoose-paginate');
var timestamps = require('mongoose-timestamp');

var schema = new mongoose.Schema({
    site_id: {
        type: String
    },
    site_url: {
        type: String
    },
    session_id: {
        type: String
    },
    browser: {
        type: String
    },
    os: {
        type: String
    },
    events_snapshots: [],


}, { usePushEach: true });
schema.plugin(paginate);
schema.plugin(timestamps);
module.exports = mongoose.model('recordings', schema);